/*
 * Music.c
 *
 *  Created on: 2 Jan 2021
 *      Author: nathan
 */


#include <stdbool.h>
#include <string.h>
#include <stdint.h>

#include "Music.h"
#include "my_main.h"


//Defines



/*Global Variables*/
volatile int16_t pcm_data[DMA_BUF_SIZE_I2S2] = {0}; //I2S out buffer

//Structures
typedef struct
{
	bool dma_i2s_TxHalfCplt;
	bool dma_i2s_TxCplt;
} flags; //Global flags to be used by interrupts etc.

//FatFs variables
volatile FIL fd;
volatile FRESULT res;
UINT bytesread;
FATFS SDFatFs;

//External variables
extern uint8_t retSD; /* Return value for SD */
extern char SDPath[4]; /* SD logical drive path */
extern FATFS SDFatFS; /* File system object for SD logical drive */
extern FIL SDFile; /* File object for SD */
extern I2S_HandleTypeDef hi2s2;

//Volatile variables
volatile flags g_flags;

/**
  * @brief  Returns the status of the i2s transfer. Can be used to check if its
  * 		safe to copy part of the buffer.
  * @param  None.
  * @retval The status of the i2s dma tx.
  * 		bit 0:  dma tx cmplt status.
  * 		bit 1: dma tx half cmplt status.
  */
uint8_t poll_music_buf_update()
{
	return (g_flags.dma_i2s_TxHalfCplt << 1) | g_flags.dma_i2s_TxCplt;
}

void play_music(char const * filename, music_file_fmt_t fmt)
{

	char const * fn = filename;

	//Check if SD card driver available (only done once)
	//
	if(retSD != 0)
	{
		printf("ERROR: SD card driver not available.");
		return 1;
	}

	printf("INFO: SD card driver available.\n");

	// Mount file system (only once after media is installed)
	//
	if((res = f_mount(&SDFatFs, (TCHAR const *)SDPath, 1)) != FR_OK)
	{
		printf("ERROR: Could not mount file system (%d).\n", res);
		return 1;
	}

	printf("INFO: Mounted file system '%s'\n", SDPath);

	// Open file
	//
	if((res = f_open(&fd, fn, FA_READ)) != FR_OK)
	{
		printf("ERROR: Opening '%s' (%d)\n", fn, res);
		f_mount(0, "", 0); // No error check
		return 1;
	}

	printf("INFO: Opened file '%s'\n", fn);

	if(fmt == wav)
	{

		f_lseek(&fd, 44);
		res = f_read(&fd, &pcm_data, sizeof(pcm_data), &bytesread);
		HAL_I2S_Transmit_DMA(&hi2s2, &pcm_data, DMA_BUF_SIZE_I2S2);
	}


}

void HAL_I2S_TxHalfCpltCallback(I2S_HandleTypeDef *hi2s)
{
	if(hi2s == &hi2s2)
	{
		g_flags.dma_i2s_TxHalfCplt = true;
		g_flags.dma_i2s_TxCplt = false;
		//Load new data into the first half of the buffer.
		//
		res = f_read(&fd, &pcm_data[0], sizeof(pcm_data)/2, &bytesread);
		if(res == FR_OK && !f_eof(&fd))
		{
			g_flags.dma_i2s_TxHalfCplt = false;
		}
		else if(f_eof(&fd))
		{
			f_lseek(&fd, 44);
		}
		else
		{
			printf("Error.. \n");
		}

	}

}

void HAL_I2S_TxCpltCallback(I2S_HandleTypeDef *hi2s)
{
	if(hi2s == &hi2s2)
	{
		g_flags.dma_i2s_TxCplt = true;
		g_flags.dma_i2s_TxHalfCplt = false;
		//Copy new data into the last half of the buffer from the sd card for
		//the I2S data buffer.
		//
		res = f_read(&fd, &pcm_data[DMA_BUF_SIZE_I2S2/2], sizeof(pcm_data)/2, &bytesread);
		if(res == FR_OK && !f_eof(&fd))
		{
			g_flags.dma_i2s_TxCplt = false;
		}
		else if(f_eof(&fd))
		{
			f_lseek(&fd, 44);
		}
		else
		{
			printf("Error.. \n");
		}

	}

}


