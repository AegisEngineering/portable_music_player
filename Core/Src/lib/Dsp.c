/*
 * Dsp.c
 *
 *  Created on: Dec 30, 2020
 *      Author: nathan
 */
#define ARM_MATH_CM4


#include <stdint.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "Dsp.h"
#include "Music.h"
#include "main.h"
#include "ST7789_320x240_16_RGB.h"
#include "arm_math.h"

#define NUM_OF_BANDS 320
#define ARM_FFT_LEN 2048

extern I2S_HandleTypeDef hi2s2;

arm_rfft_fast_instance_f32 varInstRfftF32;

static uint8_t g_band_magnitudes[NUM_OF_BANDS] = {0};

/******************************************************************************
 * Function signatures
 ******************************************************************************/

static void generate_random_magnitudes();
static float cmplxABS(float re, float im);
static void draw_spectrum();

/******************************************************************************
 * Public functions
 ******************************************************************************/
void dsp_init()
{
	arm_rfft_fast_init_f32(&varInstRfftF32, ARM_FFT_LEN);
}

void draw_spectral_lines()
{


	int16_t * i2s_buf = hi2s2.pTxBuffPtr;
	static float i2s_buf_cpy[DMA_BUF_SIZE_I2S2] = {0};
	static float i2s_buf_fft[DMA_BUF_SIZE_I2S2] = {0};

	//Copy samples from the I2S dma buffer into another buffer
	//so a FFT can be performed.
	//WARNING: arm_rfft_fast_f32() modifies the original buffer. This is why
	//a copy of the I2S buffer is made.
	//
	__disable_irq();
	switch(poll_music_buf_update())
	{

	//dma hasn't completed a transfer at all.
	case 0b00 :
		//return;
		break;

	//dma has fully completed a transfer. Last half is being
	//loaded with new data. So copy it before new data is written.
	case 0b01 :
		for(int i = DMA_BUF_SIZE_I2S2 / 2; i < DMA_BUF_SIZE_I2S2; i++)
		{
			i2s_buf_cpy[i] = (float) i2s_buf[i];
		}
		break;

	//dma has half completed a transfer. The last half of the
	//buf is about to be transfered.
	case 0b10 :
		for(int i = 0; i < DMA_BUF_SIZE_I2S2 / 2; i++)
		{
			i2s_buf_cpy[i] = (float) i2s_buf[i];
		}
		return;
		break;

	//dma has fully completed a transfer. Last half of the
	//buffer is about to be loaded with new data.
	case 0b11 :
		break;

	default:
		Error_Handler();
	}
	__enable_irq();

	arm_rfft_fast_f32(&varInstRfftF32, i2s_buf_cpy, i2s_buf_fft, 0);

	//Compute frequency magnitudes in dB for each bin
	//NOTE: for Fs = 44.1khz, and N = 2048 the frequency resolution
	//		is Fs/N ~= 21.53hz/bin.
	//		Computes the two sided spectrum.
	//
	int16_t freq_mag[ARM_FFT_LEN / 2] = {0};

	for(int i = 0, j = 0; i < ARM_FFT_LEN; i += 2, j++)
	{
		const int NOISE_FLOOR = 70;
		const int mag_floor = 1;
		float abs = cmplxABS(i2s_buf_fft[i], i2s_buf_fft[i+1]);
		freq_mag[j] = (int16_t) (20 * log10f(abs)) - NOISE_FLOOR;

		//adjust the value for noise floor correction
		//
		if(freq_mag[j] < mag_floor)
		{
			freq_mag[j] = mag_floor;
		}

	}

	//Add the two halves together to form a one sided spectrum.
	//
	uint16_t fft_onesided_mag[ARM_FFT_LEN / 4] = {0};

	for(int i = 0; i < ARM_FFT_LEN / 4; i++)
	{
		const int RHS_OFFSET = (ARM_FFT_LEN / 2) - 1;
		fft_onesided_mag[i] = freq_mag[i] + freq_mag[RHS_OFFSET - i];
	}

#if 0
	//display only the one sided spectrum
	//mag scaler adjusts the amplitude as needed.
	//

	//Copy bass 20 - 300hz
	//
	const int FREQ_RES = 44100 / ARM_FFT_LEN;
	for(int i = 0; i < X_MAX / 4; i++)
	{
		fft_onesided_mag[i] = freq_mag[i] + freq_mag[RHS_OFFSET - i];
	}
	//Copy mid 300hz - 4kHz
	//
	for(int i = 0; i < X_MAX / 4 + 80; i++)
	{

	}
	//Copy treble > 4khz
	//
	for(int i = 0; i < X_MAX / 4 + 160; i++)
	{

	}
#endif

#if 1
	//display only the one sided spectrum
	//mag scaler adjusts the amplitude as needed.
	//
	const int mag_scaler = 1;
	for(int i = 0; i < NUM_OF_BANDS; i += 2)
	{

		int fft_index = i * ((ARM_FFT_LEN / 4) / NUM_OF_BANDS);
		g_band_magnitudes[i] = fft_onesided_mag[fft_index] * mag_scaler;

		//Limit the magnitude peak so it doesn't draw on the full LCD y-axis.
		//
		if(g_band_magnitudes[i] > 210)
		{
			g_band_magnitudes[i] = 210;
		}
	}



#endif
	draw_spectrum();
}
/******************************************************************************
 * Private helper functions
 ******************************************************************************/
static void draw_spectrum()
{
	const uint8_t step_size = X_MAX/NUM_OF_BANDS;
	const uint16_t X_OFFSET = 0;
	const uint16_t Y_OFFSET = 0;
	const uint16_t BACKGROUND_COLOUR = BLACK;
	const DOT_PIXEL PIXEL_SIZE = DOT_PIXEL_1X1;


	//Draw lines on the screen in an optimized manner for each band.
	//NOTE: Only updates the changes instead of redrawing a whole line.
	//
	for(int i = 0; i < sizeof(g_band_magnitudes); i++)
	{
		//dy = difference in length of the new line segment.
		//xv = x co-ordinate of where to draw the vertical line.
		//yv = y co-ordinate of where to draw the vertical line.
		//h =  length of the vertical line to draw.
		//
		static uint16_t cache[X_MAX] = {0};
		static int16_t xv = 0;
		static int16_t yv = 0;
		static int16_t h = 0;
		int16_t dy = g_band_magnitudes[i] - cache[i];


		//Negative change in length so need to subtract part of the line
		//segment by drawing a line the same colour as the background.
		//
		if(dy < 0)
		{
			//remove minus sign from dy for better readability
			//(we only care about the magnitude)
			//
			h = -dy;
			xv = X_OFFSET + step_size * i;
			yv = cache[xv] - h;

			LCD_Draw_V_Line(xv, yv,	h, BACKGROUND_COLOUR, PIXEL_SIZE);
		}

		//Positive change so need to add to the line segment by drawing a line
		//the same colour as the background.
		//
		else if(dy > 0)
		{
			h = dy;
			xv = X_OFFSET + step_size * i;
			yv = cache[xv];
			LCD_Draw_V_Line(xv, yv, h, GREEN, PIXEL_SIZE);
		}

		//No change so draw the whole line (occurs on first run of the program).
		//
		else
		{
			h = g_band_magnitudes[i];
			xv = X_OFFSET + step_size * i;
			yv = Y_OFFSET;
			LCD_Draw_V_Line(xv, yv, h, GREEN, PIXEL_SIZE);
		}

		cache[xv] = g_band_magnitudes[i];

		//Limit the update rate for better visual experience
		//16ms Delay ~ 1/60hz (screen refresh rate)
		//
		//HAL_Delay(16);
	}
}

static void generate_random_magnitudes()
{
	//populate test frequency values
	for(int i=0; i < sizeof(g_band_magnitudes); i++)
	{
		g_band_magnitudes[i] = drand48() * Y_MAX;
	}

}

//@brief - Complex the magnitude of a complex number.
//@param re - real operand of complex number.
//@param im - imaginary operand of complex number.
//@return - the magnitude.
static float cmplxABS(float re, float im)
{
	return sqrtf(re * re + im * im);
}

/******************************************************************************
 * UNUSED
 ******************************************************************************/


#if defined(USE_MSGEQ7)

#define ADC_BUF_SIZE 2048

extern ADC_HandleTypeDef hadc1;

volatile static uint16_t adc_msgeq7_dc[ADC_BUF_SIZE] = {0};
volatile static int dc_val = 0;

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef * hadc)
{
	if(hadc == &hadc1)
	{

		for(int i = 0; i < ADC_BUF_SIZE; i++)
		{
			dc_val += adc_msgeq7_dc[i];
		}

		dc_val /= ADC_BUF_SIZE;
		HAL_ADC_Start_DMA(&hadc1, (uint32_t *) adc_msgeq7_dc, ADC_BUF_SIZE);
	}
}

void MSGEQ7_Init()
{
	//Reset the multiplexer
	HAL_GPIO_WritePin(MSGEQ7_Reset_GPIO_Port, MSGEQ7_Reset_Pin, GPIO_PIN_SET);
	HAL_Delay(5);

	//Enable the strobe pin on the MSGEQ7
	HAL_GPIO_WritePin(MSGEQ7_Reset_GPIO_Port, MSGEQ7_Reset_Pin, GPIO_PIN_RESET);
	HAL_Delay(5);

	//Leading edge of strobe selects 63Hz
	HAL_GPIO_WritePin(MSGEQ7_Strobe_GPIO_Port, MSGEQ7_Strobe_Pin, GPIO_PIN_SET);

}

void MSGEQ7_Read_Bands(uint8_t * p_band_array)
{
	const uint16_t MAX_DC_OUTPUT = 2900; //2.9v @ 200mv pk-pk input
	const uint16_t NOISE_FLOOR = 230; //230mV @ No input
	const uint8_t NUM_OF_BANDS = 7;

	for(int i = 0; i < NUM_OF_BANDS; i++)
	{
		//Measure the DC value of the band according to MSGEQ7 timing.
		//
		HAL_Delay(10);
		HAL_GPIO_WritePin(MSGEQ7_Strobe_GPIO_Port, MSGEQ7_Strobe_Pin, GPIO_PIN_RESET);

		HAL_Delay(10);
		HAL_GPIO_WritePin(MSGEQ7_Strobe_GPIO_Port, MSGEQ7_Strobe_Pin, GPIO_PIN_SET);

	}

}
#endif
