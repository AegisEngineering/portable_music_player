/*
 * ST7789_320x240_16_RGB.c
 *
 *  Created on: Dec 28, 2020
 *      Author: Nathan Fittler-Willis
 *
 *      Adapted and optimized for the STM32F411 and my music player
 *      project, based on the Waveshare 320 x 240 RGB LCD library.
 *      https://www.waveshare.com/wiki/2inch_LCD_Module
 *
 */


#include "ST7789_320x240_16_RGB.h"

/******************************************************************************
 * External variables
******************************************************************************/
extern SPI_HandleTypeDef hspi1;
extern TIM_HandleTypeDef htim3;

/******************************************************************************
 * Internal defines
******************************************************************************/
//GPIO config
#define DEV_RST_PIN     	             ST7789_RST_GPIO_Port,	ST7789_RST_Pin
#define DEV_DC_PIN      	             ST7789_DC_GPIO_Port,	ST7789_DC_Pin
#define DEV_CS_PIN			             ST7789_CS_GPIO_Port,	ST7789_CS_Pin
#define DEV_BL_PIN			             TIM3->CCR2

//GPIO read and write
#define DEV_Digital_Write(_pin, _value)  HAL_GPIO_WritePin(_pin, _value == 0 ?\
		                                 GPIO_PIN_RESET : GPIO_PIN_SET)

#define DEV_Digital_Read(_pin)           HAL_GPIO_ReadPin(_pin)

//delay x ms
#define DEV_Delay_ms(__xms)              HAL_Delay(__xms)

//PWM_BL
#define DEV_Set_PWM(_Value)              DEV_BL_PIN = _Value

//ST7789VW Commands
#define RAMWR                            0x2C  //Memory Write
#define CASET 							 0x2A  //Column Address Set
#define RASET							 0x2B  //Row Address Set
#define MADCTL                           0x36  //Memory Data Access Control

/******************************************************************************
 * Private helper function signatures
******************************************************************************/
static void LCD_Write_Command(UBYTE data);
static void LCD_WriteData_Byte(UBYTE data);
static void LCD_WriteData_Word(UWORD data);
static void LCD_SetCursor(UWORD x, UWORD y);
static void LCD_SetWindow(UWORD Xstart, UWORD Ystart, UWORD Xend, UWORD  Yend);
static void LCD_Reset(void);

/******************************************************************************
 * Public functions
******************************************************************************/

//Initialize the LCD (ST7789 320x240)
void LCD_Init(void)
{
	LCD_Reset();

	LCD_Write_Command(0x36);   //MADCTL - Mem data access control
	LCD_WriteData_Byte(0x00);  //0b 0

	LCD_Write_Command(0x3A);  //COLMOD - Interface pixel format
	LCD_WriteData_Byte(0x05); //0b 0101 - 16-bit/pixel 5 6 5

	LCD_Write_Command(0x21);  //INVON - Display inversion on

	LCD_Write_Command(0x2A);  //DISPON - Display on
	LCD_WriteData_Byte(0x00); //0b 0
	LCD_WriteData_Byte(0x00); //0b 0
	LCD_WriteData_Byte(0x01); //0b 1
	LCD_WriteData_Byte(0x3F); //0b 0011 1111

	LCD_Write_Command(0x2B);  //RASET - Row address set
	LCD_WriteData_Byte(0x00); //0b 0
	LCD_WriteData_Byte(0x00); //0b 0
	LCD_WriteData_Byte(0x00); //0b 0
	LCD_WriteData_Byte(0xEF); //0b 1110 1111

	LCD_Write_Command(0xB2);  //PORCTRL - Porch control
	LCD_WriteData_Byte(0x0C); //0b 1100
	LCD_WriteData_Byte(0x0C); //0b 1100
	LCD_WriteData_Byte(0x00); //0b 0
	LCD_WriteData_Byte(0x33); //0b 0011 0011
	LCD_WriteData_Byte(0x33); //0b 0011 0011

	LCD_Write_Command(0xB7);  //GCTRL - Gate control
	LCD_WriteData_Byte(0x35); //0b 0011 0101

	LCD_Write_Command(0xBB);  //VCOMS - VCOM Setting
	LCD_WriteData_Byte(0x1F); //0b 0001 1111

	LCD_Write_Command(0xC0);  //LCMCTRL - LCM Control
	LCD_WriteData_Byte(0x2C); //

	LCD_Write_Command(0xC2);  //VDVVRHEN - VDV and VRH cmd enable
	LCD_WriteData_Byte(0x01); //

	LCD_Write_Command(0xC3);  //VRHS - VRH set
	LCD_WriteData_Byte(0x12); //

	LCD_Write_Command(0xC4);  //VDVSET - VDV Setting
	LCD_WriteData_Byte(0x20); //

	LCD_Write_Command(0xC6);  //FRCTR2 - FR Control 2
	LCD_WriteData_Byte(0x0F); // 60hz

	LCD_Write_Command(0xD0);  //PWCTRL1 - Power Control 1
	LCD_WriteData_Byte(0xA4); //
	LCD_WriteData_Byte(0xA1); //

	LCD_Write_Command(0xE0);  //PVGAMCTRL - Positive voltage gamma control
	LCD_WriteData_Byte(0xD0); //
	LCD_WriteData_Byte(0x08); //
	LCD_WriteData_Byte(0x11); //
	LCD_WriteData_Byte(0x08); //
	LCD_WriteData_Byte(0x0C); //
	LCD_WriteData_Byte(0x15); //
	LCD_WriteData_Byte(0x39); //
	LCD_WriteData_Byte(0x33); //
	LCD_WriteData_Byte(0x50); //
	LCD_WriteData_Byte(0x36); //
	LCD_WriteData_Byte(0x13); //
	LCD_WriteData_Byte(0x14); //
	LCD_WriteData_Byte(0x29); //
	LCD_WriteData_Byte(0x2D); //

	LCD_Write_Command(0xE1);  //NVGAMCTRL - Negative Voltage Gamma Control
	LCD_WriteData_Byte(0xD0); //
	LCD_WriteData_Byte(0x08); //
	LCD_WriteData_Byte(0x10); //
	LCD_WriteData_Byte(0x08); //
	LCD_WriteData_Byte(0x06); //
	LCD_WriteData_Byte(0x06); //
	LCD_WriteData_Byte(0x39); //
	LCD_WriteData_Byte(0x44); //
	LCD_WriteData_Byte(0x51); //
	LCD_WriteData_Byte(0x0B); //
	LCD_WriteData_Byte(0x16); //
	LCD_WriteData_Byte(0x14); //
	LCD_WriteData_Byte(0x2F); //
	LCD_WriteData_Byte(0x31); //
	LCD_Write_Command(0x21);  //

	LCD_Write_Command(0x11);  //SLPOUT - Sleep out

	//Adjust the display such that the 320 side becomes the x-axis
	//and the 240 side becomes the y-axis, the origin is near the pinout
	//of the LCD.
	//
	LCD_Write_Command(MADCTL);
	LCD_WriteData_Byte(0b001 << 5);

	LCD_Write_Command(0x29);  //DSPON - Display on
}

void LCD_Draw_V_Line(UWORD x, UWORD y, UWORD h, UWORD colour, DOT_PIXEL pixel_size)
{
	//Prevent overshoot on the screen
	//
	if(x > X_MAX)
	{
		x = X_MAX;
	}
	if(y > Y_MAX)
	{
		y = Y_MAX;
	}

	LCD_SetWindow(x, y, x + pixel_size, y + h);
	while(h--)
	{
		LCD_WriteData_Word(colour);
	}

}

void LCD_Draw_Point(UWORD x, UWORD y, UWORD colour, DOT_PIXEL pixel_size)
{
	LCD_Draw_Window(x, y, x + pixel_size, y + pixel_size, colour);

}

void LCD_Draw_Window(UWORD x_start, UWORD y_start,
		UWORD x_end, UWORD y_end, UWORD colour)
{
	LCD_SetWindow(x_start, y_start, x_end, y_end);
	uint32_t area = (x_end - x_start) * (y_end - y_start);

	while(area--)
	{
		LCD_WriteData_Word(colour);
	}

}
/******************************************************************************
 * Private helper functions
******************************************************************************/

void DEV_SPI_WriteByte(UBYTE _dat)
{
	HAL_SPI_Transmit(&hspi1, (uint8_t *) &_dat, 1, 500);
}

static void LCD_Write_Command(UBYTE data)
{
	DEV_Digital_Write(DEV_CS_PIN, 0);
	DEV_Digital_Write(DEV_DC_PIN, 0);
	DEV_SPI_WriteByte(data);
}

static void LCD_WriteData_Byte(UBYTE data)
{
	DEV_Digital_Write(DEV_CS_PIN, 0);
	DEV_Digital_Write(DEV_DC_PIN, 1);
	DEV_SPI_WriteByte(data);
	DEV_Digital_Write(DEV_CS_PIN,1);
}

static void LCD_WriteData_Word(UWORD data)
{
	DEV_Digital_Write(DEV_CS_PIN, 0);
	DEV_Digital_Write(DEV_DC_PIN, 1);
	DEV_SPI_WriteByte((data>>8) & 0xff);
	DEV_SPI_WriteByte(data);
	DEV_Digital_Write(DEV_CS_PIN, 1);
}

static void LCD_SetCursor(UWORD x, UWORD y)
{
	//Set the x position
	LCD_Write_Command(CASET);
	LCD_WriteData_Byte(x >> 8);
	LCD_WriteData_Byte(x);
	LCD_WriteData_Byte(x >> 8);
	LCD_WriteData_Byte(x);

	//Set the y position
	LCD_Write_Command(RASET);
	LCD_WriteData_Byte(y >> 8);
	LCD_WriteData_Byte(y);
	LCD_WriteData_Byte(y >> 8);
	LCD_WriteData_Byte(y);

	//Begin sending RGB data
	LCD_Write_Command(RAMWR);
}

static void LCD_SetWindow(UWORD Xstart, UWORD Ystart, UWORD Xend, UWORD  Yend)
{
	if(Xend > X_MAX || Yend > Y_MAX)
	{
		return;
	}

	//Set the column address
	LCD_Write_Command(CASET);
	LCD_WriteData_Byte(Xstart >> 8);
	LCD_WriteData_Byte(Xstart & 0xff);
	LCD_WriteData_Byte((Xend - 1) >> 8);
	LCD_WriteData_Byte((Xend - 1) & 0xff);

	//Set the row address
	LCD_Write_Command(RASET);
	LCD_WriteData_Byte(Ystart >> 8);
	LCD_WriteData_Byte(Ystart & 0xff);
	LCD_WriteData_Byte((Yend - 1) >> 8);
	LCD_WriteData_Byte((Yend - 1) & 0xff);

	//Begin sending RGB data
	LCD_Write_Command(RAMWR);
}

static void LCD_Reset(void)
{
	DEV_Digital_Write(DEV_CS_PIN, 1);
	DEV_Delay_ms(200);
	DEV_Digital_Write(DEV_CS_PIN, 0);
	DEV_Delay_ms(200);
	DEV_Digital_Write(DEV_RST_PIN, 0);
	DEV_Delay_ms(200);
	DEV_Digital_Write(DEV_RST_PIN, 1);
	DEV_Delay_ms(200);
}
