/*
 * my_main.c
 *
 *  Created on: Dec 6, 2020
 *      Author: nathan
 */

//Includes
#include <stdbool.h>
#include <string.h>

#include "my_main.h"
#include "ST7789_320x240_16_RGB.h"
#include "Dsp.h"
#include "Music.h"



//const TCHAR *fn = "Aspekt- Hi Jack 16-bit.wav";
//const TCHAR *fn = "Aspekt- Hi Jack 32-bit.wav";
//const TCHAR *fn = "Fictivision vs. Phynn - Escape (Phynn Mix) 16-bit.wav";
//const TCHAR *fn = "Fictivision vs. Phynn - Escape (Phynn Mix) 32-bit cut.wav";
//const TCHAR *fn = "Fictivision vs. Phynn - Escape (Phynn Mix) 16-bit cut.wav";
//const TCHAR *fn = "1kHz_44100Hz_16bit_05sec.wav";
//const TCHAR *fn = "CSC_sweep_20-20k.wav";
const TCHAR *fn = "Realms of Chaos.wav";
//const TCHAR *fn = "Lisa Miskovsky - Still Alive (Airwalker Dubstep Remix).wav";



void main_init(void)
{
	LCD_Init();
	LCD_Draw_Window(0, 0, X_MAX, Y_MAX, BLACK);
	dsp_init();

	play_music(fn, wav);
}





void main_loop(void)
{

	draw_spectral_lines();

#if 0

	  FATFS SDFatFs;
	  //const TCHAR *fn = "1kHz_44100Hz_16bit_05sec.wav";//"hellotext.txt";
	  const TCHAR *fn = "FIRST.TXT";//"hellotext.txt";

	  FIL fd;
	  FRESULT res;
	  UINT bytesread;
	  uint8_t textread[BUFF_SIZE] = {0};

	  // Welcome message
	  //
	  printf("FatFs API Example\n");

	  // Check if SD card driver available (only done once)
	  //
	  if(retSD != 0)
	  {
	    printf("ERROR: SD card driver not available.");
	    return 1;
	  }
	  printf("INFO: SD card driver available.\n");

	  // Mount file system (only once after media is installed)
	  //
	  if((res = f_mount(&SDFatFs, (TCHAR const *)SDPath, 1)) != FR_OK)
	  {
	    printf("ERROR: Could not mount file system (%d).\n", res);
		return 1;
	  }
	  printf("INFO: Mounted file system '%s'\n", SDPath);

	  // Open file
	  //
	  if((res = f_open(&fd, fn, FA_READ)) != FR_OK)
	  {
	    printf("ERROR: Opening '%s' (%d)\n", fn, res);
	    f_mount(0, "", 0); // No error check
	    return 1;
	  }
	  printf("INFO: Opened file '%s'\n", fn);

	  // Read data from file
	  //
	  if ((res = f_read(&fd, &textread, BUFF_SIZE-1, &bytesread)) != FR_OK)
	  {
	    printf("ERROR: Reading '%s' (%d)\n", fn, res);
	    f_close(&fd);      // No error check
	    f_mount(0, "", 0); // No error check
	    return 1;
	  }
	  textread[bytesread] = '\0'; // Null terminate string (assume text in file)
	  printf("INFO: Read '%s'\n", textread);

	  // Close file
	  //
	  if ((res = f_close(&fd)) != FR_OK)
	  {
	    printf("ERROR: closing '%s' (%d)\n", fn, res);
	    f_mount(0, "", 0); // No error check
	    return 1;
	  }
	  printf("INFO: File '%s' closed\n", fn);

	  // Unmount file system (only required if media is removed)
	  //
	  if ((res = f_mount(0, "", 0)) != FR_OK)
	  {
	    printf("ERROR: Unmounting file system\n");
	    return 1;
	  }
	  printf("INFO: File system unmounted\n");

	  // End of example
	  //
	  printf("End of example.\n");

#endif


}
