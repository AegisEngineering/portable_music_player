/*
 * Dsp.h
 *
 *  Created on: Dec 30, 2020
 *      Author: nathan
 */

#ifndef INC_LIB_DSP_H_
#define INC_LIB_DSP_H_

void dsp_init();
void draw_spectral_lines();

#if defined(USE_MSGEQ7)

void MSGEQ7_Read_Bands(uint8_t * p_band_array);
void MSGEQ7_Init();

#endif

#endif /* INC_LIB_DSP_H_ */
