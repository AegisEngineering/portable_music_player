/*
 * ST7789_320x240_16_RGB.h
 *
 *  Created on: Dec 28, 2020
 *      Author: nathan
 */

#ifndef INC_LIB_ST7789_320X240_16_RGB_H_
#define INC_LIB_ST7789_320X240_16_RGB_H_

#include "stdint.h"
#include "stm32f4xx_hal.h"
#include "main.h"

#define UBYTE   uint8_t
#define UWORD   uint16_t
#define UDOUBLE uint32_t

#define X_MAX  320         //LCD x-axis max
#define Y_MAX  240         //LCD y-axis max

/**
 * The size of the point
 **/
typedef enum
{
	DOT_PIXEL_1X1  = 1,		// 1 x 1
	DOT_PIXEL_2X2  , 		// 2 X 2
	DOT_PIXEL_3X3  ,		// 3 X 3
	DOT_PIXEL_4X4  ,		// 4 X 4
	DOT_PIXEL_5X5  , 		// 5 X 5
	DOT_PIXEL_6X6  , 		// 6 X 6
	DOT_PIXEL_7X7  , 		// 7 X 7
	DOT_PIXEL_8X8  , 		// 8 X 8

} DOT_PIXEL;

//Colours
//
#define WHITE					0xFFFF
#define BLACK					0x0000
#define BLUE 					0x001F
#define BRED 					0XF81F
#define GRED 					0XFFE0
#define GBLUE					0X07FF
#define RED  					0xF800
#define MAGENTA				    0xF81F
#define GREEN					0x07E0
#define CYAN 					0x7FFF
#define YELLOW				    0xFFE0
#define BROWN					0XBC40
#define BRRED					0XFC07
#define GRAY 					0X8430
#define DARKBLUE			    0X01CF
#define LIGHTBLUE			    0X7D7C
#define GRAYBLUE                0X5458
#define LIGHTGREEN              0X841F
#define LGRAY 			        0XC618
#define LGRAYBLUE               0XA651
#define LBBLUE                  0X2B12

void LCD_Init(void);
void LCD_Draw_Window(UWORD x_start, UWORD y_start, UWORD x_end, UWORD y_end, UWORD colour);
void LCD_Draw_V_Line(UWORD x, UWORD y, UWORD h, UWORD colour, DOT_PIXEL pixel_size);
void LCD_Draw_Point(UWORD x, UWORD y, UWORD colour, DOT_PIXEL pixel_size);

#endif /* INC_LIB_ST7789_320X240_16_RGB_H_ */
