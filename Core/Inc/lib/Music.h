/*
 * Music.h
 *
 *  Created on: 2 Jan 2021
 *      Author: nathan
 */

#ifndef INC_LIB_MUSIC_H_
#define INC_LIB_MUSIC_H_

#define DMA_BUF_SIZE_I2S2 2048  //I2S2 out buffer size

#include <stdbool.h>

//Only wav file format is supported for now.
//
typedef enum
{
	wav

} music_file_fmt_t;


void play_music(char const * filename, music_file_fmt_t fmt);
uint8_t poll_music_buf_update();

#endif /* INC_LIB_MUSIC_H_ */
