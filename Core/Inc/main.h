/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */



/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MSGEQ7_Strobe_Pin GPIO_PIN_0
#define MSGEQ7_Strobe_GPIO_Port GPIOC
#define MSGEQ7_DC_In_Pin GPIO_PIN_1
#define MSGEQ7_DC_In_GPIO_Port GPIOC
#define Test_Measure_Signal_Pin GPIO_PIN_4
#define Test_Measure_Signal_GPIO_Port GPIOC
#define MSGEQ7_Reset_Pin GPIO_PIN_0
#define MSGEQ7_Reset_GPIO_Port GPIOB
#define ST7789_RST_Pin GPIO_PIN_13
#define ST7789_RST_GPIO_Port GPIOB
#define SD_Detect_Pin GPIO_PIN_6
#define SD_Detect_GPIO_Port GPIOC
#define ST7789_CS_Pin GPIO_PIN_10
#define ST7789_CS_GPIO_Port GPIOA
#define ST7789_DC_Pin GPIO_PIN_5
#define ST7789_DC_GPIO_Port GPIOB
#define GG_SCL_Pin GPIO_PIN_6
#define GG_SCL_GPIO_Port GPIOB
#define GG_SDA_Pin GPIO_PIN_7
#define GG_SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
