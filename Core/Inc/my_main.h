/*
 * my_main.h
 *
 *  Created on: Dec 6, 2020
 *      Author: natha
 */

#ifndef INC_MY_MAIN_H_
#define INC_MY_MAIN_H_

#include "main.h"
#include "fatfs.h"
#include "stm32f4xx_hal.h"

void main_init(void);
void main_loop(void);

#endif /* INC_MY_MAIN_H_ */
